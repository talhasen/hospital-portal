<html>
<head>
  <title>Reservation</title>
</head>
<body>
<?php  
		$date=date("d.m.Y");// brings the current system date
		list($minday,$minmonth,$year)=explode(".",$date);
		$mindate=$year."-".$minmonth."-".$minday;
		$maxday=$minday;
		$maxmonth=$minmonth;
		if($minday<=15){
			$maxday=$maxday+15;
		}else{
			$maxday=$maxday-15;
			$maxmonth++;
		}
		if($maxmonth==13){
			$maxmonth=1;
			$year++;
		}
		if($maxmonth<10)
			$maxmonth="0".$maxmonth;
		if($maxday<10)
			$maxday="0".$maxday;
		$maxdate=$year."-".$maxmonth."-".$maxday; 
?>
	<form name="form" action="deleteisdone.php" method="post">
	<table border="2">
		<tr>
		<td colspan="2"><h3 align="center">Appointment Deletion</h3></td>
		</tr>
		<tr>
		<td>Patient ID</td>
		<td><input type="text" name="DeletePatientID"/><br>Please, type the patient ID of the patient you would like to delete!</td>
		</tr>
		<tr>
		<td>Appointment Date</td>
		<td><input type="date" name="DeleteAppointmentDate" min="<?php echo $mindate ?>" max="<?php echo $maxdate ?>"/><br>Please, type the appointment date of the patient you would like to delete!</td>
		</tr>
		<tr>
		<td colspan="2"><input type="submit" name="delete" value="Delete"/></td>
		</tr>
	</table>
	</form>

</body>
</html>