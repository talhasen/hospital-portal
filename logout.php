<?php
  session_start();
  ob_start();
  session_destroy();
  echo "Signing out...\nMainpage is loading...";
  header("Refresh: 1; url=index.php");
  ob_end_flush();
?>