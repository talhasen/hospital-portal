<?php
if(isset($_POST['PatientID']) && isset($_POST['PatientPassword'])) {
	$PatientID = $_POST['PatientID'];
	$PatientPassword = $_POST['PatientPassword'];
	if(empty($PatientID) || empty($PatientPassword)) {
      echo 'Please, fill the two fields both';
    }else {
		require("./connect.php");
		ob_start();
		$query=mysql_query("select PatientID,PatientPassword from password");
		if(mysql_num_rows($query)!=0){
			while($row = mysql_fetch_assoc($query))
			{
				if(($PatientID==$row["PatientID"]) and ($PatientPassword==$row["PatientPassword"])){
					session_start();
					
					$_SESSION["login"] = "true";
					$_SESSION["user"] = $PatientID;
					$_SESSION["pass"] = $PatientPassword;
					header("Location:patientuser.php?PatientID=".$PatientID."");
				}else{
					echo "Incorrect PatientID or PatientPassword<br>";
					echo "Login Page is loading<br>";
					header("Refresh: 2; url=index.php");
				}
			}
		}
		ob_end_flush();
		mysql_close($connection);
	}
}
if(isset($_POST['SpecialistID']) && isset($_POST['SpecialistPassword'])) {
	$SpecialistID = $_POST['SpecialistID'];
	$SpecialistPassword = $_POST['SpecialistPassword'];
	if(empty($SpecialistID) || empty($SpecialistPassword)) {
      echo 'Please, fill the two fields both';
    }else {
		require("./connect.php");
		ob_start();
		$query=mysql_query("select SpecialistID,SpecialistPassword from password");
		if(mysql_num_rows($query)!=0){
			while($row = mysql_fetch_assoc($query)){
				if(($SpecialistID==$row["SpecialistID"]) and ($SpecialistPassword==$row["SpecialistPassword"])){
					session_start();
					
					$_SESSION["login"] = "true";
					$_SESSION["user"] = $SpecialistID;
					$_SESSION["pass"] = $SpecialistPassword;
					header("Location:specialistuser.php?SpecialistID=".$SpecialistID."");
				}else{
					echo "Incorrect SpecialistID or SpecialistPassword<br>";
					echo "Login Page is loading<br>";
					header("Refresh: 2; url=index.php");
				}
			}
		}
		ob_end_flush();
		mysql_close($connection);
	}
}
if(isset($_POST['AdminName']) && isset($_POST['AdminPassword'])) {
	$AdminName = $_POST['AdminName'];
	$AdminPassword = $_POST['AdminPassword'];
	if(empty($AdminName) || empty($AdminPassword)) {
      echo 'Please, fill the two fields both';
    }else {
		ob_start();
		$xml = simplexml_load_file("admin.xml");
		$condition=false;
		foreach ($xml->admin as $admin){
			if($admin->name == $AdminName && $admin->password == $AdminPassword && $condition == false){
				session_start();
					$_SESSION["login"] = "true";
					$_SESSION["user"] = $AdminName;
					$_SESSION["pass"] = $AdminPassword;
					header("Location:adminuser.php?AdminName=".$AdminName."");
			}
			if(!$condition){
					echo "Incorrect AdminName or AdminPassword<br>";
					echo "Login Page is loading<br>";
					header("Refresh: 2; url=index.php");
			}
		}
		ob_end_flush();
	}
}

?>